package com.t1.yd.tm.api.service;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    void clear(String userId);

    Project create(final String name);

    Project create(final String name, final String description);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

}
