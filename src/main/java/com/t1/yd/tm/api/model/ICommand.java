package com.t1.yd.tm.api.model;

import com.t1.yd.tm.enumerated.Role;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

    Role[] getRoles();

}
