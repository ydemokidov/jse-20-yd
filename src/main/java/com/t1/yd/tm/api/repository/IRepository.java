package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity);

    void clear();

    List<E> findAll();

    List<E> findAll(Comparator comparator);

    E findOneById(String id);

    E findOneByIndex(Integer index);

    E removeById(String id);

    E removeByIndex(Integer index);

    boolean existsById(String id);

    int getSize();

}
