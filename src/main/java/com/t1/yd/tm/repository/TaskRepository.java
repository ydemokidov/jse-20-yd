package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> foundTasks = new ArrayList<>();
        for (final Task task : entities) {
            if (!task.getUserId().equals(userId)) continue;
            if (task.getProjectId() != null && task.getProjectId().equals(projectId)) {
                foundTasks.add(task);
            }
        }
        return foundTasks;
    }
}
