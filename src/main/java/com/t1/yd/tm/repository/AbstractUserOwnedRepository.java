package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IUserOwnedRepository;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<E extends AbstractUserOwnedEntity> extends AbstractRepository<E> implements IUserOwnedRepository<E> {

    @Override
    public E add(final String userId, final E entity) {
        entity.setUserId(userId);
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear(final String userId) {
        final List<E> foundEntities = findAll(userId);
        entities.removeAll(foundEntities);
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        final List<E> result = new ArrayList<>();
        for (final E entity : entities) {
            if (entity.getUserId().equals(userId)) result.add(entity);
        }
        return result;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator comparator) {
        final List<E> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public E findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        for (final E entity : entities) {
            if (!entity.getId().equals(id)) continue;
            if (!entity.getUserId().equals(userId)) continue;
            return entity;
        }
        return null;
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findOneById(userId, id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final List<E> userEntities = findAll(userId);
        final E entityToRemove = userEntities.get(index);
        userEntities.remove(entityToRemove);
        return entityToRemove;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(final String userId) {
        return findAll(userId).size();
    }
}
