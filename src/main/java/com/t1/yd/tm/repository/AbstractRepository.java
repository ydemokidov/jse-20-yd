package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IRepository;
import com.t1.yd.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public List<E> findAll(final Comparator comparator) {
        final List<E> result = new ArrayList<>(entities);
        result.sort(comparator);
        return result;
    }

    @Override
    public E findOneById(final String id) {
        for (final E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findOneByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public E removeById(final String id) {
        final E entity = findOneById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final Integer index) {
        final E entity = findOneByIndex(index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    public int getSize() {
        return entities.size();
    }

}
