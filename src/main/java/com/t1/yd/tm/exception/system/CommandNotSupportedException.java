package com.t1.yd.tm.exception.system;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command is not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command " + command + " is not supported...");
    }

}
